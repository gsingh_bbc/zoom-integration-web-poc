﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Zoom.Demo.Token;

namespace Zoom.Demo.Handler
{
    public class ZoomAuthHandler : DelegatingHandler
    {
        private readonly ITokenService tokenService;

        public ZoomAuthHandler(ITokenService tokenService)
        {
            this.tokenService = tokenService;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            request = await SetRequestHeaders(request);
            return await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }

        private async Task<HttpRequestMessage> SetRequestHeaders(HttpRequestMessage request)
        {
            var token = await tokenService.GetToken();
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return request;
        }
    }
}