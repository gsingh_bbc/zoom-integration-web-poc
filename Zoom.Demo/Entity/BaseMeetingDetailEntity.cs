﻿using System;
using Newtonsoft.Json;

namespace Zoom.Demo.Entity
{
    public class BaseMeetingDetailEntity
    {
        public BaseMeetingDetailEntity()
        {
            Settings = new SettingsEntity();
        }

        [JsonProperty("uuid")] public string uuid { get; set; }
        [JsonProperty("id")] public long id { get; set; }
        [JsonProperty("host_id")] public string HostId { get; set; }
        [JsonProperty("topic")] public string Topic { get; set; }
        [JsonProperty("type")] public int MeeingType { get; set; }
        [JsonProperty("status")] public string Status { get; set; }
        [JsonProperty("start_time")] public DateTime StartTime { get; set; }
        [JsonProperty("duration")] public int Duration { get; set; }
        [JsonProperty("timezone")] public string Timezone { get; set; }
        [JsonProperty("agenda")] public string Agenda { get; set; }
        [JsonProperty("created_at")] public DateTime CreatedAt { get; set; }
        [JsonProperty("join_url")] public string JoinURL { get; set; }
        [JsonProperty("start_url")] public string StartURL { get; set; }
        [JsonProperty("password")] public string Password { get; set; }
        [JsonProperty("registration_url")] public string RegistrationURL { get; set; }
        [JsonProperty("settings")] public SettingsEntity Settings { get; set; }
    }
}