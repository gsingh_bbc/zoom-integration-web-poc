﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Zoom.Demo.Models;

namespace Zoom.Demo.Entity
{
    public class UserMeetings : SearchResults
    {
        public UserMeetings()
        {
            Meetings = new List<MeetingDetailEntity>();
        }

        [JsonProperty("meetings")] public List<MeetingDetailEntity> Meetings { get; set; }
    }

    public class MeetingEntity
    {
        [JsonProperty("uuid")] public string uuid { get; set; }
        [JsonProperty("id")] public long id { get; set; }
        [JsonProperty("host_id")] public string HostId { get; set; }
        [JsonProperty("topic")] public string Topic { get; set; }
        [JsonProperty("type")] public int MeeingType { get; set; }
        [JsonProperty("start_time")] public DateTime StartTime { get; set; }
        [JsonProperty("duration")] public int Duration { get; set; }
        [JsonProperty("timezone")] public string Timezone { get; set; }
        [JsonProperty("agenda")] public string Agenda { get; set; }
        [JsonProperty("created_at")] public DateTime CreatedAt { get; set; }
        [JsonProperty("join_url")] public string JoinURL { get; set; }
    }
}