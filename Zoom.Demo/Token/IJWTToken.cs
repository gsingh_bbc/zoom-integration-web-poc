﻿using Zoom.Demo.Models;

namespace Zoom.Demo.Token
{
    public interface IJWTToken
    {
        ZoomToken GetToken();
    }
}