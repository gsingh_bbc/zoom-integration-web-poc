﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using LazyCache;
using Newtonsoft.Json.Linq;
using Zoom.Demo.Token;

namespace Zoom.Demo
{
    public class ZoomHttpClient : TokenService, IZoomHttpClient
    {
        private readonly HttpClient client;

        public ZoomHttpClient(HttpClient httpclient) : base(new CachingService())
        {
            client = httpclient;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri("https://api.zoom.us/v2/");
        }

        public async Task<TResponse> PostAsync<TRequest, TResponse>(
            string url,
            TRequest input)
        {
            return await CreateRequest<TResponse>(url, HttpMethod.Post, input);
        }

        public async Task<TResponse> GetAsync<TResponse>(
            string url)
        {
            return await CreateRequest<TResponse>(url, HttpMethod.Get);
        }

        public async Task<TResponse> PutAsync<TRequest, TResponse>(
            string url,
            TRequest input)
        {
            return await CreateRequest<TResponse>(url, HttpMethod.Put, input);
        }

        public async Task<TResponse> DeleteAsync<TResponse>(
            string url,
            string token = null)
        {
            return await CreateRequest<TResponse>(url, HttpMethod.Delete, token);
        }

        #region Private

        async Task<TResponse> CreateRequest<TResponse>(
            string url,
            HttpMethod method)
        {
            return await CreateRequestMessage(url, method, async msg => { return await GetResult<TResponse>(msg); });
        }

        async Task<TResponse> CreateRequest<TResponse>(
            string url,
            HttpMethod method,
            object input)
        {
            return await CreateRequestMessage(url, method, async msg =>
            {
                using (var content = new StringContent(JObject.FromObject(input).ToString()))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    msg.Content = content;
                    return await GetResult<TResponse>(msg);
                }
            });
        }

        async Task<TResponse> CreateRequestMessage<TResponse>(
            string url,
            HttpMethod method,
            Func<HttpRequestMessage, Task<TResponse>> functor)
        {
            using (var msg = new HttpRequestMessage())
            {
                msg.RequestUri = new Uri(client.BaseAddress + url);
                msg.Method = method;
                msg.Headers.Authorization = new AuthenticationHeaderValue("Bearer", await GetToken());
                return await functor(msg);
            }
        }

        async Task<TResponse> GetResult<TResponse>(HttpRequestMessage msg)
        {
            using (var response = await client.SendAsync(msg))
            {
                using (var content = response.Content)
                {
                    var responseContent = await content.ReadAsStringAsync();
                    if (!response.IsSuccessStatusCode)
                        throw new Exception(responseContent);
                    if (typeof(IConvertible).IsAssignableFrom(typeof(TResponse)))
                        return (TResponse) Convert.ChangeType(responseContent, typeof(TResponse));
                    return JToken.Parse(responseContent).ToObject<TResponse>();
                }
            }
        }

        #endregion
    }
}