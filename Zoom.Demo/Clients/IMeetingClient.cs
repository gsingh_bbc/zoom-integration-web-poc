﻿using System.Threading.Tasks;
using Zoom.Demo.Entity;
using Zoom.Demo.Helper;

namespace Zoom.Demo
{
    public interface IMeetingClient
    {
        Task<UserMeetings> GetUserMeetings();
        Task<MeetingRegistrants> GetMeetingRegistrants(long meetingId);

        Task<UserMeetings> GetUserMeetings(string userId,
            string status = Constants.ZoomType.UserMeetingType.Live, bool includeRegistrations = false, int pageSize = 30, int pageNumber = 1);
    }
}