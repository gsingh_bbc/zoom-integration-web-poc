﻿using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;
using Zoom.Demo;

namespace Demo.Zoom.Web.Controllers
{
    public class MeetingController : Controller
    {
        private readonly IMeetingClient meetingClient;

        public MeetingController(IMeetingClient meetingClient)
        {
            this.meetingClient = meetingClient;
        }

        // GET: Meeting
        public async Task<ActionResult> Index()
        {
            var userMeeting =
                await meetingClient.GetUserMeetings(ConfigurationManager.AppSettings["userId"], "upcoming",true);
            return View(userMeeting);
        }

        public ActionResult ListAll()
        {
            return View();
        }
    }
}