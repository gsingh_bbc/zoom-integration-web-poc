﻿using System;
using System.Configuration;

namespace Zoom.Demo
{
    public class AppConfig
    {
        public string ApiProtocol => GetSetting("zoom-client-option:https");
        public string ApiHost => GetSetting("zoom-client-option:apiHost");
        public string ApiVersion => GetSetting("zoom-client-option:apiVersion");
        public string ClientId => GetSetting("zoom-client-option:clientId");
        public string ClientSecret => GetSetting("zoom-client-option:clientSecret");
        public string ClientCode => GetSetting("zoom-client-option:clientCode");
        public string ApiSecret => GetSetting("zoom-client-option:apiSecret");
        public string ApiKey => GetSetting("zoom-client-option:apiKey");
        public bool UseJWTAppToken => Convert.ToBoolean(GetSetting("useJWTAppToken"));


        // Wrapper around ConfigurationManager.AppSettings call
        protected string GetSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}