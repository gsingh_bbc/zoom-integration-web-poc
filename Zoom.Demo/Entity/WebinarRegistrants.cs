﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Zoom.Demo.Models;

namespace Zoom.Demo.Entity
{
   public class WebinarRegistrants : SearchResults
    {
        [JsonProperty("registrants")] public List<WebinarRegistrantEntity> Registrants { get; set; }
    }
}
