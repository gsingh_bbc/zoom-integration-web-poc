﻿using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace Demo.Zoom.Web.Helper
{
    /// <summary>
    ///     Generate signature for zoom
    ///     <ref>https://marketplace.zoom.us/docs/sdk/native-sdks/web/essential/signature</ref>
    /// </summary>
    public static class Signature
    {
        static readonly char[] padding = {'='};

        public static string GetToken(string meetingNumber = "", string roldeId = "1")
        {
            var apiKey = ConfigurationManager.AppSettings["zoom-client-option:apiKey"];
            var apiSecret = ConfigurationManager.AppSettings["zoom-client-option:apiSecret"];
            var ts = (ToTimestamp(DateTime.UtcNow.ToUniversalTime()) - 30000).ToString();
            return GenerateToken(apiKey, apiSecret, meetingNumber, ts, roldeId);
        }

        private static long ToTimestamp(DateTime value)
        {
            var epoch = (value.Ticks - 621355968000000000) / 10000;
            return epoch;
        }

        private static string GenerateToken(string apiKey, string apiSecret, string meetingNumber, string ts,
            string role)
        {
            var message = $"{apiKey}{meetingNumber}{ts}{role}";
            apiSecret = apiSecret ?? "";
            var encoding = new ASCIIEncoding();
            var keyByte = encoding.GetBytes(apiSecret);
            var messageBytesTest = encoding.GetBytes(message);
            var msgHashPreHmac = Convert.ToBase64String(messageBytesTest);
            var messageBytes = encoding.GetBytes(msgHashPreHmac);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                var hashmessage = hmacsha256.ComputeHash(messageBytes);
                var msgHash = Convert.ToBase64String(hashmessage);
                var token = $"{apiKey}.{meetingNumber}.{ts}.{role}.{msgHash}";
                var tokenBytes = Encoding.UTF8.GetBytes(token);
                return Convert.ToBase64String(tokenBytes).TrimEnd(padding);
            }
        }
    }
}