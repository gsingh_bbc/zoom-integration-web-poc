﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Zoom.Demo.Entity
{
   public class BaseRegistrantEntity
    {
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("first_name")] public string FirstName { get; set; }
        [JsonProperty("last_name")] public string LastName { get; set; }
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("address")] public string Address { get; set; }
        [JsonProperty("city")] public string City { get; set; }
        [JsonProperty("country")] public string Country { get; set; }
        [JsonProperty("zip")] public string Zip { get; set; }
        [JsonProperty("state")] public string State { get; set; }
        [JsonProperty("phone")] public string Phone { get; set; }
        [JsonProperty("industry")] public string Industry { get; set; }
        [JsonProperty("org")] public string Org { get; set; }
        [JsonProperty("job_title")] public string JobTitle { get; set; }

        [JsonProperty("purchasing_time_frame")]
        public string PurchasingTimeFrame { get; set; }

        [JsonProperty("role_in_purchase_process")]
        public string RoleInPurchaseProcess { get; set; }

        [JsonProperty("no_of_employees")] public string NoOfEmployees { get; set; }
        [JsonProperty("comments")] public string Comments { get; set; }
        [JsonProperty("custom_questions")] public CustomQuestionsEntity[] CustomQuestions { get; set; }
        [JsonProperty("status")] public string Status { get; set; }
        [JsonProperty("create_time")] public DateTime CreateTime { get; set; }
        [JsonProperty("join_url")] public string JoinURL { get; set; }
    }
}
