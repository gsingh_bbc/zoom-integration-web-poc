﻿using System.Configuration;
using System.Web.Mvc;
using Demo.Zoom.Web.Helper;
using Demo.Zoom.Web.Models;

namespace Demo.Zoom.Web.Controllers
{
    /// <summary>
    ///     Zoom Meeting Controller : Integrated zoom SDK via CDN
    ///     <ref>https://marketplace.zoom.us/docs/sdk/native-sdks/web/getting-started/integrate</ref>
    /// </summary>
    public class ZoomController : Controller
    {
        // GET: Zoom
        public ActionResult Index()
        {
            var model = new ZoomConfigurationModel()
            {
                ApiKey = ConfigurationManager.AppSettings["zoom-client-option:apiKey"],
                ApiSecret = ConfigurationManager.AppSettings["zoom-client-option:apiSecret"]
            };
            return View(model);
        }

        public ActionResult Webinar()
        {
            var model = new ZoomConfigurationModel()
            {
                ApiKey = ConfigurationManager.AppSettings["zoom-client-option:apiKey"],
                ApiSecret = ConfigurationManager.AppSettings["zoom-client-option:apiSecret"]
            };
            return View(model);
        }

        [HttpGet]
        public JsonResult GetSignature(string meetingId, string roleId)
        {
            var data = new {result = Signature.GetToken(meetingId, roleId)};
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}