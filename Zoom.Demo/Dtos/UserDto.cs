﻿using System;

namespace Zoom.Demo.Dtos
{
    public class UserDto
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }
        public string RoleName { get; set; }
        public long PMI { get; set; }
        public bool UsePMI { get; set; }
        public string Timezone { get; set; }
        public int Verified { get; set; }
        public string Department { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastLoginTime { get; set; }
        public string[] IMGroupIds { get; set; }
        public string Status { get; set; }
        public string PersonalMeetingUrl { get; set; }
        public string HostKey { get; set; }
        public string JId { get; set; }
        public string[] GroupIds { get; set; }
        public string AccountId { get; set; }
        public string Language { get; set; }
        public string PhoneCountry { get; set; }
        public string PhoneNumber { get; set; }
    }
}