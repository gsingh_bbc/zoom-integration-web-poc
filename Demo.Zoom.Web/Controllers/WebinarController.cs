﻿using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;
using Zoom.Demo.Clients;

namespace Demo.Zoom.Web.Controllers
{
    public class WebinarController : Controller
    {
        private readonly IWebinarClient webinarClient;

        public WebinarController(IWebinarClient webinarClient)
        {
            this.webinarClient = webinarClient;
        }

        // GET: Webinar
        public async Task<ActionResult> Index()
        {
            var webinars = await webinarClient.GetUserWebinars(ConfigurationManager.AppSettings["userId"], true,true);
            return View(webinars);
        }
    }
}