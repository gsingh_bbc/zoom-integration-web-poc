﻿using System.Collections.Generic;

namespace Zoom.Demo.Entity
{
    public class WebinarDetailEntity : BaseMeetingDetailEntity
    {
        public WebinarDetailEntity()
        {
            Registrants = new List<WebinarRegistrantEntity>();
        }
        public List<WebinarRegistrantEntity> Registrants { get; set; }
    }
}