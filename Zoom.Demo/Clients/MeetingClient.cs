﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Zoom.Demo.Entity;
using Zoom.Demo.Helper;

namespace Zoom.Demo
{
    public class MeetingClient : IMeetingClient
    {
        private readonly IZoomHttpClient client;

        public MeetingClient(IZoomHttpClient httpclient)
        {
            client = httpclient;
        }

        public async Task<UserMeetings> GetUserMeetings(string userId,
            string status = Constants.ZoomType.UserMeetingType.Live, bool includeRegistrations = false, int pageSize = 30, int pageNumber = 1)
        {
            string url = string.Format(Constants.ZoomURL.Users.GetUserMeeting, userId, status, pageSize, pageNumber);
            var meetings = await client.GetAsync<UserMeetings>(url);
            var result = new UserMeetings
            {
                PageCount = meetings.PageCount,
                PageNumber = meetings.PageNumber,
                PageSize = meetings.PageSize,
                TotalRecords = meetings.TotalRecords
            };
            foreach (var meeting in meetings.Meetings)
            {
                var meetingDetail = await GetMeetingDetail(meeting.id);
                if (includeRegistrations)
                {
                    var registrants = await GetMeetingRegistrants(meeting.id);
                    if (registrants.HasValue)
                    {
                        meetingDetail.Registrants = registrants.Registrants;
                    }
                }
               
                result.Meetings.Add(meetingDetail);
            }

            return result;
        }

        public async Task<UserMeetings> GetUserMeetings()
        {
            return await client.GetAsync<UserMeetings>(Constants.ZoomURL.MeContext.GetMeetings);
        }

        public async Task<MeetingRegistrants> GetMeetingRegistrants(long meetingId)
        {
            try
            {
                return await client.GetAsync<MeetingRegistrants>(
                    string.Format(Constants.ZoomURL.Meetings.GetMeetingRegistrants, meetingId));
            }
            catch (Exception ex )
            {

                var response = JsonConvert.DeserializeObject<ZoomExceptionResponse>(ex.Message);
                if (response.Code == 300)
                {
                    return new MeetingRegistrants();
                }
                throw;
            }
            
        }

        public async Task<MeetingDetailEntity> GetMeetingDetail(long meetingId)
        {
            return await client.GetAsync<MeetingDetailEntity>(
                string.Format(Constants.ZoomURL.Meetings.GetMeetingDetail, meetingId));
        }
    }
}