﻿using Newtonsoft.Json;

namespace Zoom.Demo.Models
{
    public class SearchResults
    {
        [JsonProperty("page_count")] public int PageCount { get; set; }
        [JsonProperty("page_number")] public string PageNumber { get; set; }
        [JsonProperty("page_size")] public string PageSize { get; set; }
        [JsonProperty("total_records")] public int TotalRecords { get; set; }

        public bool HasValue
        {
            get { return TotalRecords > 0; }
        }
    }
}