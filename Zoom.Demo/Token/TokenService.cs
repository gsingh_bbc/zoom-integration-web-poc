﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LazyCache;
using Newtonsoft.Json;
using Zoom.Demo.Helper;
using Zoom.Demo.Models;

namespace Zoom.Demo.Token
{
    /// <summary>
    ///     Service to get access token and revoking the token from zoom
    ///     <ref>https://marketplace.zoom.us/docs/guides/auth/oauth</ref>
    /// </summary>
    public class TokenService : TokenCache, ITokenService
    {
        // private IJWTToken jwtToken;
        public TokenService(IAppCache cache) : base(cache)
        {
            //jwtToken = new JWTToken();
        }

        public async Task<string> GetToken()
        {
            var token = await RetrieveTokenDataFromCache();
            if (!token.IsValidAndNotExpiring)
            {
                token = await GetAccessToken(token, true);
            }

            return token.AccessToken;
        }

        protected override async Task<ZoomToken> GetAccessToken(ZoomToken token = null, bool getRefreshToken = false)
        {
            if (token == null)
                token = new ZoomToken();

            var config = new AppConfig();

            if (config.UseJWTAppToken)
            {
                return JWTToken.GetToken();
            }

            var client_id = config.ClientId;
            var client_secret = config.ClientSecret;
            var clientCreds = Encoding.UTF8.GetBytes($"{client_id}:{client_secret}");
            var postMessage = new Dictionary<string, string>();
            if (getRefreshToken)
            {
                postMessage.Add("grant_type", "refresh_token");
                postMessage.Add("refresh_token", token.RefreshToken);
            }
            else
            {
                postMessage.Add("grant_type", "authorization_code");
                postMessage.Add("redirect_uri", Constants.ZoomURL.Redirect);
                postMessage.Add("code", config.ClientCode);
            }

            var response = await Send(clientCreds, postMessage);

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                token = JsonConvert.DeserializeObject<ZoomToken>(json);
                token.ExpiresAt = DateTime.UtcNow.AddSeconds(token.ExpiresIn - 300);
                //token.ExpiresAt = DateTime.UtcNow.AddSeconds(60);
            }
            else
            {
                throw new ApplicationException("Unable to retrieve access token from Zoom");
            }

            return token;
        }

        private static async Task<HttpResponseMessage> Send(byte[] clientCreds, Dictionary<string, string> postMessage)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", Convert.ToBase64String(clientCreds));


            var request = new HttpRequestMessage(HttpMethod.Post, Constants.ZoomURL.Token)
            {
                Content = new FormUrlEncodedContent(postMessage)
            };

            var response = await client.SendAsync(request);
            return response;
        }

        //private async Task<ZoomToken> RetrieveTokenDataFromCache()
        //{
        //    var mediaRightsCache = cache.Get<ZoomToken>(ZoomTokenCacheKey);
        //    if (mediaRightsCache == null)
        //    {
        //        var data = await GetAccessToken();
        //        if (data != null)
        //        {
        //            cache.Remove(ZoomTokenCacheKey);
        //            cache.Add(ZoomTokenCacheKey, data);
        //        }
        //    }

        //    return cache.Get<ZoomToken>(ZoomTokenCacheKey); ;
        //}

        //private async Task<ZoomToken> RetrieveTokenDataFromCache()
        //{
        //    ObjectCache cache = MemoryCache.Default;

        //    if (cache.Contains(ZoomTokenCacheKey))
        //    {
        //        return cache.Get(ZoomTokenCacheKey) as ZoomToken;
        //    }

        //    var token = await GetAccessToken();
        //    var cacheItemPolicy = new CacheItemPolicy {AbsoluteExpiration = token.ExpiresAt};
        //    cache.Add(ZoomTokenCacheKey, token, cacheItemPolicy);
        //    return token;


        //}
    }
}