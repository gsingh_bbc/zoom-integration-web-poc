﻿using System.Collections.Generic;

namespace Zoom.Demo.Entity
{
    public class MeetingDetailEntity : BaseMeetingDetailEntity
    {
        public MeetingDetailEntity()
        {
            this.Registrants= new List<MeetingRegistrantEntity>();
        }
        public List<MeetingRegistrantEntity> Registrants { get; set; }
    }
}