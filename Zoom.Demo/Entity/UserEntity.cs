﻿using System;
using Newtonsoft.Json;

namespace Zoom.Demo.Entity
{
    public class UserEntity
    {
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("first_name")] public string FirstName { get; set; }
        [JsonProperty("last_name")] public string LastName { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("type")] public int Type { get; set; }
        [JsonProperty("role_name")] string RoleName { get; set; }
        [JsonProperty("pmi")] public long PMI { get; set; }
        [JsonProperty("use_pmi")] public bool UsePMI { get; set; }
        [JsonProperty("timezone")] public string Timezone { get; set; }
        [JsonProperty("verified")] public int Verified { get; set; }
        [JsonProperty("dept")] public string Department { get; set; }
        [JsonProperty("created_at")] public DateTime CreatedAt { get; set; }
        [JsonProperty("last_login_time")] public DateTime LastLoginTime { get; set; }
        [JsonProperty("im_group_ids")] public string[] IMGroupIds { get; set; }
        [JsonProperty("status")] public string Status { get; set; }
        [JsonProperty("personal_meeting_url")] public string PersonalMeetingUrl { get; set; }
        [JsonProperty("host_key")] public string HostKey { get; set; }
        [JsonProperty("jid")] public string JId { get; set; }
        [JsonProperty("group_ids")] public object[] GroupIds { get; set; }
        [JsonProperty("account_id")] public string AccountId { get; set; }
        [JsonProperty("language")] public string Language { get; set; }
        [JsonProperty("phone_country")] string PhoneCountry { get; set; }
        [JsonProperty("phone_number")] public string PhoneNumber { get; set; }
    }
}