﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Zoom.Demo.Models;

namespace Zoom.Demo.Token
{
    public static class JWTToken //: IJWTToken
    {
        public static ZoomToken GetToken()
        {
            // Token will be good for 60 minutes
            DateTime Expiry = DateTime.UtcNow.AddMinutes(60);

            var config = new AppConfig();
            string ApiKey = config.ApiKey;
            string ApiSecret = config.ApiSecret;

            int ts = (int) (Expiry - new DateTime(1970, 1, 1)).TotalSeconds;

            // Create Security key  using private key above:
            var securityKey =
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ApiSecret));

            // length should be >256b
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Finally create a Token
            var header = new JwtHeader(credentials);

            //Zoom Required Payload
            var payload = new JwtPayload
            {
                {"iss", ApiKey},
                {"exp", ts}
            };

            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();

            // Token to String so you can use it in your client
            var tokenString = handler.WriteToken(secToken);

            return new ZoomToken
            {
                AccessToken = tokenString,
                ExpiresIn = Expiry.Second,
                ExpiresAt = DateTime.UtcNow.AddSeconds(Expiry.Second - 300)
            };
        }
    }
}