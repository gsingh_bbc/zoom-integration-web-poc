﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zoom.Demo.Dtos;
using Zoom.Demo.Models;

namespace Zoom.Demo
{
    public interface IUserClient
    {
        Task<User> SearchUser(string status, int roleid, int pageNumber = 1, int pageSize = 30);
        Task<UserDto> GetUser();
        Task<List<UserObj>> GetAllUser();
    }
}