﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Zoom.Demo.Entity
{
   public class CustomQuestionsEntity
    {
        [JsonProperty("title")] public string Title { get; set; }
        [JsonProperty("value")] public string Value { get; set; }
    }
}
