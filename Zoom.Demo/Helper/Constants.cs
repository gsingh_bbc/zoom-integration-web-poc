﻿namespace Zoom.Demo.Helper
{
    internal static class Constants
    {
        internal const string HttpHeaderAuthorization = "Authorization";
        internal const string ZoomTokenCacheKey = "zoom-token";

        internal static class ZoomURL
        {
            internal static readonly string Token = "https://zoom.us/oauth/token";
            internal static readonly string Redirect = "https://webhook.site/7e34430e-a0c2-4ec0-a9aa-cfc9c25e3a30";

            /// <summary>
            ///     Link of me context API's
            ///     https://marketplace.zoom.us/docs/guides/auth/oauth#me-context
            /// </summary>
            internal static class MeContext
            {
                internal static readonly string GetUser = "users/me";
                internal static readonly string GetMeetings = "users/me/meetings";
                internal static readonly string GetWebinars = "users/me/webinars";
            }

            internal static class Users
            {
                internal static readonly string GetUserMeeting =
                    "users/{0}/meetings?type={1}&page_size={2}&page_number={3}";

                internal static readonly string GetUserWebinars = "users/{0}/webinars?page_size={1}&page_number={2}";
            }

            internal static class Meetings
            {
                internal static readonly string GetMeetingRegistrants =
                    "meetings/{0}/registrants?status=approved&page_size=30&page_number=1";

                internal static readonly string GetMeetingDetail = "meetings/{0}?show_previous_occurrences=true";
            }

            internal static class Webinars
            {
                internal static readonly string GetWebinarDetail = "webinars/{0}?show_previous_occurrences=true";
                internal static readonly string GetWebinarRegistrants =
                    "webinars/{0}/registrants?status={1}&page_size={2}&page_number={3}";
            }
        }

        internal static class ZoomType
        {
            internal static class UserMeetingType
            {
                internal const string Live = "live";
                internal const string UpComing = "upcoming";
                internal const string Scheduled = "scheduled";
            }
        }
    }
}