﻿using System.Threading.Tasks;
using LazyCache;
using Zoom.Demo.Helper;
using Zoom.Demo.Models;

namespace Zoom.Demo.Token
{
    /// <summary>
    ///     Cache service uses Lazy cache to store and fetch the token
    ///     <ref>https://github.com/alastairtree/LazyCache/wiki</ref>
    /// </summary>
    public abstract class TokenCache
    {
        private readonly IAppCache cache;

        protected TokenCache(IAppCache cache)
        {
            this.cache = cache;
        }

        protected async Task<ZoomToken> RetrieveTokenDataFromCache()
        {
            var tokenCache = cache.Get<ZoomToken>(Constants.ZoomTokenCacheKey);
            if (tokenCache == null)
            {
                var data = await GetAccessToken();
                if (data != null)
                {
                    cache.Remove(Constants.ZoomTokenCacheKey);
                    cache.Add(Constants.ZoomTokenCacheKey, data);
                    return data;
                }
            }

            return tokenCache;
        }

        protected abstract Task<ZoomToken> GetAccessToken(ZoomToken token = null, bool getRefreshToken = false);
    }
}