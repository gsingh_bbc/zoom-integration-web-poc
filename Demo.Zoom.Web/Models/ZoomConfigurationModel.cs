﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Zoom.Web.Models
{
    public class ZoomConfigurationModel
    {
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
    }
}