﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Zoom.Demo.Models
{
    public class User : SearchResults
    {
        public User()
        {
            Users = new List<UserObj>();
        }

        [JsonProperty("users")] public List<UserObj> Users { get; set; }
    }

    public class UserObj
    {
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("first_name")] public string FirstName { get; set; }
        [JsonProperty("last_name")] public string LastName { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("type")] public int Type { get; set; }
        [JsonProperty("pmi")] public long PMI { get; set; }
        [JsonProperty("timezone")] public string Timezone { get; set; }
        [JsonProperty("verified")] public int Verified { get; set; }
        [JsonProperty("dept")] public string Department { get; set; }
        [JsonProperty("created_at")] public DateTime CreatedAt { get; set; }
        [JsonProperty("last_login_time")] public DateTime LastLoginTime { get; set; }
        [JsonProperty("last_client_version")] public string LastClientVersion { get; set; }
        [JsonProperty("pic_url")] public string PicURL { get; set; }
        [JsonProperty("im_group_ids")] public string[] IMGroupIds { get; set; }
        [JsonProperty("status")] public string Status { get; set; }
    }
}