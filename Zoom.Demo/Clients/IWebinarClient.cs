﻿using System.Threading.Tasks;
using Zoom.Demo.Entity;

namespace Zoom.Demo.Clients
{
    public interface IWebinarClient
    {
        Task<UserWebinars> GetUserWebinars(string userId, int pageSize = 30,
            int pageNumber = 1);

        Task<UserWebinars> GetUserWebinars(string userId, bool showOnlyUpComing = false, int pageSize = 30,
            int pageNumber = 1);

        Task<UserWebinars> GetUserWebinars(string userId, bool showOnlyUpComing = false, bool includeRegistrant = false,
            int pageSize = 30,
            int pageNumber = 1);

        Task<WebinarDetailEntity> GetWebinarDetail(long webinarId);
    }
}