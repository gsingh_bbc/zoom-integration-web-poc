﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Zoom.Demo.Models;

namespace Zoom.Demo.Entity
{
    public class UserWebinars : SearchResults
    {
        public UserWebinars()
        {
            Webinars = new List<WebinarDetailEntity>();
        }

        [JsonProperty("webinars")] public List<WebinarDetailEntity> Webinars { get; set; }
    }
}