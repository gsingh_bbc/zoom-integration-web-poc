﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Zoom.Demo.Dtos;
using Zoom.Demo.Entity;
using Zoom.Demo.Helper;
using Zoom.Demo.Models;

namespace Zoom.Demo
{
    public class UserClient : IUserClient
    {
        private readonly IZoomHttpClient client;

        public UserClient(IZoomHttpClient httpclient)
        {
            client = httpclient;
        }

        public async Task<User> SearchUser(string status, int roleid, int pageNumber = 1, int pageSize = 30)
        {
            string url = $"users?status={status}&page_size={pageSize}&page_number={pageNumber}&role_id={roleid}";
            var data = await client.GetAsync<User>(url);
            return data;
        }

        public async Task<List<UserObj>> GetAllUser()
        {
            var users = new List<UserObj>();
            int pageNumber = 1;
            int pageSize = 300;
            //string url = $"users?page_size={pageSize}&page_number={pageNumber}";
            //var data = await client.GetAsync<User>(url);
            //if (Convert.ToInt32(data.TotalRecords) > 0)
            //{
            //    users.AddRange(data.Users);
            //}
            int totalPages = 1;
            do
            {
                string url = $"users?page_size={pageSize}&page_number={pageNumber}";
                var data = await client.GetAsync<User>(url);
                if (Convert.ToInt32(data.TotalRecords) > 0)
                {
                    totalPages = data.PageCount;
                    users.AddRange(data.Users);
                }

                pageNumber++;
            } while (pageNumber < totalPages);

            return users;
        }

        public async Task<UserDto> GetUser()
        {
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<UserEntity, UserDto>(); });
            var iMapper = config.CreateMapper();

            return iMapper.Map<UserEntity, UserDto>(
                await client.GetAsync<UserEntity>(Constants.ZoomURL.MeContext.GetUser));
        }
    }
}