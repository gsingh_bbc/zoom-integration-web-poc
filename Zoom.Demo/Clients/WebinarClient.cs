﻿using System;
using System.Threading.Tasks;
using Zoom.Demo.Entity;
using Zoom.Demo.Helper;

namespace Zoom.Demo.Clients
{
    public class WebinarClient : IWebinarClient
    {
        private readonly IZoomHttpClient client;

        public WebinarClient(IZoomHttpClient client)
        {
            this.client = client;
        }

        public async Task<UserWebinars> GetUserWebinars(string userId, int pageSize = 30,
            int pageNumber = 1)
        {
            string url = string.Format(Constants.ZoomURL.Users.GetUserWebinars, userId, pageSize, pageNumber);
            return await client.GetAsync<UserWebinars>(url);

        }

        public async Task<UserWebinars> GetUserWebinars(string userId, bool showOnlyUpComing = false, int pageSize = 30,
            int pageNumber = 1)
        {

            var webinars = await GetUserWebinars(userId, pageSize, pageNumber);
            var result = new UserWebinars
            {
                PageCount = webinars.PageCount,
                PageNumber = webinars.PageNumber,
                PageSize = webinars.PageSize,
                TotalRecords = webinars.TotalRecords
            };
            foreach (var webinar in webinars.Webinars)
            {
                var meetingDetail = await GetWebinarDetail(webinar.id);
                if (showOnlyUpComing)
                {
                    if (meetingDetail.StartTime.AddMinutes(meetingDetail.Duration).Date >= DateTime.Now.Date)
                        result.Webinars.Add(meetingDetail);
                }
                else
                {
                    result.Webinars.Add(meetingDetail);
                }
            }

            return result;
        }

        public async Task<UserWebinars> GetUserWebinars(string userId, bool showOnlyUpComing = false, bool includeRegistrant = false, int pageSize = 30,
            int pageNumber = 1)
        {

            if (showOnlyUpComing && !includeRegistrant)
            {
                return await GetUserWebinars(userId, showOnlyUpComing, pageSize, pageNumber);
            }

            var webinars = await GetUserWebinars(userId, pageSize, pageNumber);
            var result = new UserWebinars
            {
                PageCount = webinars.PageCount,
                PageNumber = webinars.PageNumber,
                PageSize = webinars.PageSize,
                TotalRecords = webinars.TotalRecords
            };
            foreach (var webinar in webinars.Webinars)
            {
                var webinarDetail = await GetWebinarDetail(webinar.id);
                if (includeRegistrant)
                {
                    var registrants = await GetWebinarRegistrants(webinar.id);

                    if (registrants != null && registrants.TotalRecords > 0)
                        webinarDetail.Registrants = registrants.Registrants;
                }


                if (showOnlyUpComing)
                {
                    if (webinarDetail.StartTime.AddMinutes(webinarDetail.Duration).Date >= DateTime.Now.Date)
                        result.Webinars.Add(webinarDetail);
                }
                else
                {
                    result.Webinars.Add(webinarDetail);
                }


            }

            return result;
        }
        public async Task<WebinarDetailEntity> GetWebinarDetail(long webinarId)
        {
            return await client.GetAsync<WebinarDetailEntity>(
                string.Format(Constants.ZoomURL.Webinars.GetWebinarDetail, webinarId));
        }

        public async Task<WebinarRegistrants> GetWebinarRegistrants(long webinarId, int pageSize = 30,
            int pageNumber = 1)
        {
            return await client.GetAsync<WebinarRegistrants>(
                string.Format(Constants.ZoomURL.Webinars.GetWebinarRegistrants, webinarId, "approved", pageSize, pageNumber));
        }
    }
}