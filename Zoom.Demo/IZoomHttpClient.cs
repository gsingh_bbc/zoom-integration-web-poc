﻿using System.Threading.Tasks;

namespace Zoom.Demo
{
    public interface IZoomHttpClient
    {
        Task<TResponse> PostAsync<TRequest, TResponse>(
            string url,
            TRequest input);

        Task<TResponse> GetAsync<TResponse>(
            string url);

        Task<TResponse> PutAsync<TRequest, TResponse>(
            string url,
            TRequest input);

        Task<TResponse> DeleteAsync<TResponse>(
            string url,
            string token = null);
    }
}