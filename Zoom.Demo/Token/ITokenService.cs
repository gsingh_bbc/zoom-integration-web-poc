﻿using System.Threading.Tasks;

namespace Zoom.Demo.Token
{
    public interface ITokenService
    {
        Task<string> GetToken();
    }
}