# Zoom Demo


  - [1. Content](#1-content)  
  - [2. Features](#2-features)
  - [3. Docs & links](#3-docs--links)
  - [4. Developer Notes](#4-developer-notes)


## 1. Content 
This is the web application developed in ASP.NET MVC framework. It has its own zoom client solution to interact with zoom api
This sample applicatio will show the list of meetings and webinars for a particular user.
There are two seprate pages created to directly join any meeting or webinar.  

## 2. Features

* Zoom Web SDk integration
* Generating signature from server side
* Zoom Auth and JWT token implementation
* Get all meeting API integration 
* Get all webinar API integration 
* Get all meeting participant API integration
* Get all webinar participant API integration 


## 3. Docs & links

* [Zoom API](https://marketplace.zoom.us/docs/api-reference/using-zoom-apis)
* [OAuth 2.0 Authentication](https://marketplace.zoom.us/docs/api-reference/using-zoom-apis#using-oauth)
* [JWT Authentication](https://marketplace.zoom.us/docs/api-reference/using-zoom-apis#using-jwt)
* [Zoom web SDK](https://marketplace.zoom.us/docs/sdk/native-sdks/web)
* [Lazy Cache](https://github.com/alastairtree/LazyCache)
* [Zoom Signature](https://marketplace.zoom.us/docs/sdk/native-sdks/web/essential/signature)

## 4. Developer Notes

* Before running the project please add the API key and API secret in web.config and in view files.

