﻿using Newtonsoft.Json;

namespace Zoom.Demo.Entity
{
    public class SettingsEntity
    {
        //public bool host_video { get; set; }
        //public bool participant_video { get; set; }
        //public bool cn_meeting { get; set; }
        //public bool in_meeting { get; set; }
        //public bool join_before_host { get; set; }
        //public bool mute_upon_entry { get; set; }
        //public bool watermark { get; set; }
        //public bool use_pmi { get; set; }
        //public int approval_type { get; set; }
        //public string audio { get; set; }
        //public string auto_recording { get; set; }
        //public bool enforce_login { get; set; }
        //public string enforce_login_domains { get; set; }
        //public string alternative_hosts { get; set; }
        //public bool close_registration { get; set; }
        //public bool registrants_confirmation_email { get; set; }
        //public bool waiting_room { get; set; }
        //public string[] global_dial_in_countries { get; set; }
        //public Global_Dial_In_Numbers[] global_dial_in_numbers { get; set; }
        [JsonProperty("contact_name")] public string ContactName { get; set; }

        [JsonProperty("contact_email")] public string ContactEmail { get; set; }
        //public bool registrants_email_notification { get; set; }
        //public bool meeting_authentication { get; set; }
    }

    //public class Global_Dial_In_Numbers
    //{
    //    public string country_name { get; set; }
    //    public string city { get; set; }
    //    public string number { get; set; }
    //    public string type { get; set; }
    //    public string country { get; set; }
    //}
}